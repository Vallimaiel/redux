import React, { Component } from 'react';
import { View, Platform, Text, StyleSheet, TouchableOpacity,Dimensions,SafeAreaView } from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons'
import {  createMaterialTopTabNavigator, tabBarOptions } from 'react-navigation';
import All from './AllTab';
import Following from './FollowingTab';
const screenWidth = Dimensions.get('window').width
import colors from '../res/colors'
import {moderateScale, myFonts} from '../res/scaling'

const TabNavigator = createMaterialTopTabNavigator({

  All: { screen: All },
  Following: { screen: Following }
},
  {
    tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'grey',
      labelStyle: {
        fontSize:myFonts.f16,
      },
      indicatorStyle: {
        backgroundColor: colors.primary
      },
      style: {
        backgroundColor: 'white',
      }

    }
  });

export default class Group extends Component {
  static router = TabNavigator.router;
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: 'D21 GROUP',
      headerTitleStyle:{ color:colors.primary,flex:1,textAlign:'center',fontSize:myFonts.f18},
        headerStyle: {
             shadowOpacity: 10,
             backgroundColor:colors.statusBar,
             height:screenWidth/8
        },
        headerLeft:(
          <TouchableOpacity style={{paddingHorizontal:moderateScale(10)}} onPress={navigation.toggleDrawer}>
            <MaterialIcon
                name="menu"
                size={Platform.OS === 'ios' ?  25 : screenWidth/16}
                color={colors.primary}
            />
            </TouchableOpacity>
        ),
        headerRight: (
           <TouchableOpacity style={{paddingRight:moderateScale(10)}} onPress={()=>navigation.navigate('Search')}>
        <MaterialIcon
            name="search"
            size={Platform.OS === 'ios' ?  25 :screenWidth/16}
            color={colors.primary}
        />
        </TouchableOpacity>)
  }
  }
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.postContainer}>
          <View style={{ justifyContent: 'center' }}>
            <MaterialIcon name='edit' size={Platform.OS === 'ios' ?  20 : screenWidth/22} style={styles.editIcon} color={colors.primary} />
          </View>
          <View style={{ flex: 1, flexWrap: 'wrap' }}>
            <Text style={styles.editText}>Inspire your group, Add your story now</Text>
          </View>
        </View>
        <TabNavigator navigation={this.props.navigation} />
      </SafeAreaView>
    )

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  postContainer:
  {
    borderWidth: 1,
    backgroundColor: colors.postBackground,
    margin: moderateScale(15),
    flexDirection: 'row'
  },
  editIcon:
  { 
  alignSelf: 'center', 
  margin: moderateScale(5) 
  },
  editText:
  { color: colors.lightGrey, 
    margin: moderateScale(5), 
    fontSize: myFonts.f16, 
    fontWeight: "100" 
  }

});