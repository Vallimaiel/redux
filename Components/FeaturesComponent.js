import React, { Component } from 'react';
import {
  StyleSheet, Text, View,Platform, Alert, FlatList, Image
  , ActivityIndicator, TouchableOpacity, ToastAndroid, ScrollView,Dimensions,SafeAreaView
} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../res/colors'
import {myFonts, moderateScale,scale} from '../res/scaling'
import StoriesData from '../Modal/Modal';
const screenWidth=Dimensions.get('window').width;



const data = [
  { key: 'CUSTOMER SUCESS',key2: 'Target 70' }, 
  { key: 'EMPLOYEE SUCCESS',key2: 'Target 100' },
  { key: 'REVENUE & PRODUCTIVITY',key2: 'Target 15% + 15%'},
  { key: 'EBIDTA & ROCE',key2: 'Target 25% + 25%' },
  { key: 'FINAL DESTINATION',key2: 'Target Top 10th %' }
];
const stories = [
  { key: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim',
    time:'1h ago'
  }, 
  { key: 'Xxcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim',
  time:'1h ago' }, 
  { key: 'Cxcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim',
  time:'1h ago' } 
  
];
export default class FeaturesComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: global.data,
      isLoading: true,
      itemCount: 2,
      isLiked:false,
    }
  }

  static navigationOptions = ({ navigation }) => {

    return {
        headerTitle: 'D21 SUMMARY',
             shadowOpacity: 10,
             headerTitleStyle:{ color:colors.primary,flex:1,textAlign:'center',fontSize:myFonts.f18},//Platform.OS === 'ios' ?  null : screenWidth/22},
        headerStyle: {
             shadowOpacity: 10,
             backgroundColor:colors.statusBar,
             height:screenWidth/8
        },
        headerLeft:(
          <TouchableOpacity style={{paddingHorizontal:moderateScale(10)}} onPress={navigation.toggleDrawer}>
            <MaterialIcon
                name="menu"
                size={Platform.OS === 'ios' ?  25 : screenWidth/16}
                color={colors.primary}
            />
            </TouchableOpacity>
        ),
        headerRight: (
           <TouchableOpacity style={{paddingRight:moderateScale(10)}} onPress={()=>navigation.navigate('Search')}>
        <MaterialIcon
            name="search"
            size={Platform.OS === 'ios' ?  25 :screenWidth/16}
            color={colors.primary}
        />
        </TouchableOpacity>)
    }
  }

  renderItem = ({ item, index }) => {
    return (
<View style={{alignItems:'center',
      justifyContent:'center',
      flexDirection:'row',
      marginBottom: moderateScale(15),
      elevation:0,
      shadowOpacity:0,
      borderWidth:1,
      borderColor:colors.d21Labels,
      borderRadius:5,
    height:Platform.OS === 'ios' ?  80 : moderateScale(80)}}> 
     {/* screenWidth/5 */}
      <View style={{flex:5,backgroundColor:colors.d21Labels}}>
      <View style={{flex:1,flexDirection:'row',marginLeft: moderateScale(15),marginRight: moderateScale(15),flexWrap:'wrap'}}> 
        <MaterialIcon name='face' size={Platform.OS === 'ios' ?  30 : screenWidth/14} style={{alignSelf:'center'}}/>
      <Text style={{flex:1,color:'black',alignSelf:'center',flexWrap:'wrap',textAlign:'left',marginLeft: moderateScale(10), fontSize:screenWidth/30}}>{item.key}</Text>
         </View>
         </View>
          <View style={{flex:4,backgroundColor:'white',flexDirection:'row',flexWrap:'wrap',padding: moderateScale(15)}}>
          <Text style={{textAlign:'center',alignSelf:'flex-start',color:colors.lightGrey, fontSize:screenWidth/24}}>{item.key2}</Text>
       </View>
       <MaterialIcon name='chevron-right' size={Platform.OS === 'ios' ?  30 : screenWidth/14} style={{flex:1,alignSelf:'center',color:'black'}}/>
</View>
    );
  };

  renderWhatsNew =  ({ item, index }) => {
    return (
      <View style ={{
      flex: 1,
      marginLeft:  moderateScale(20),
      marginRight: moderateScale(20),
      marginBottom: moderateScale(10),
      marginTop: moderateScale(2)}}>
        <Text  numberOfLines={2} style={{fontSize:myFonts.f14,color:colors.primary,lineHeight:Platform.OS === 'ios' ?  30 :screenWidth/14}}  >{item.key} </Text>
        <Text style={{textAlign:"right",color:colors.primary,marginTop: moderateScale(15),marginBottom: moderateScale(10),fontSize:myFonts.f12}}>{item.time}</Text>
        <View style={{ width: '100%', height: 1, backgroundColor: colors.seperator, alignSelf: 'center' }}>
         </View>
      </View>
    );
  };

  renderStories = ({ item }) => {
    global.StoriesData = 'helloooooooooooo'
    console.log(global.StoriesData)
    return (
      <View style={{}}>
     { console.log('rendering stories')}
        <TouchableOpacity style={styles.imageContainer}
          onPress={
            () => {
              ToastAndroid.show(item.book_title, ToastAndroid.SHORT)
              // this.props.navigation.navigate('Detail', { itemId: item.book_title, value: item.author })
            }
          }>
          <Image source={require('../Assets/nature2.jpeg') } resizeMode="cover" style={styles.image} />
          <Text numberOfLines={2} style={styles.description} >{item.book_title}</Text>

        </TouchableOpacity>
        <View style={styles.buttonsRow}>
          <View style={styles.likeAndComment}>
          <TouchableOpacity style={styles.buttonContainer} onPress={() => this.setState( {isLiked: !this.state.isLiked})}>
            <MaterialIcon name={this.state.isLiked ? 'favorite' : 'favorite-border'} size={Platform.OS === 'ios' ?  18 :screenWidth/22} style={styles.buttonIcon} />
            <Text style={styles.buttonText}>908</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonContainer}  onPress={() =>  Alert.alert('Alert', "456 comments!!")} >
            <MaterialIcon name='comment' size={Platform.OS === 'ios' ?  18 :screenWidth/22} style={styles.buttonIcon}/>
            <Text style={styles.buttonText}>456</Text>
          </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.share} onPress={() => Alert.alert('Alert', "Share to!!")} >
            <Text style={[styles.buttonText]}> SHARE</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }

  renderSeperator = () => {
    return (
      <View style={{ width: '80%', height: 1, backgroundColor: colors.seperator, alignSelf: 'center' }}>
      </View>
    )
  }
  myKeyExtractor = (item, index) => item.book_title;


  componentDidMount() {
   // const url = 'http://www.json-generator.com/api/json/get/ccLAsEcOSq?indent=1'
    console.log('mounting')
    const url = 'http://www.json-generator.com/api/json/get/ccLAsEcOSq?indent=1'
    console.log(global.loadedd)
    if(global.loadedd == 'false')
    {
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        // this.setState({
        //   dataSource: responseJson.book_array,
        //   isLoading: false
        // })
        global.loadedd = 'true'
        global.data = responseJson.book_array;
        this.setState({
             dataSource: global.data,
             isLoading: false
           })
        console.log(global.data)
      })
      .catch((error) => {
        console.log(error)
      })
    }
    }
  render() {
    return(
      
      !global.loadedd ?
          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
           {console.log("*******")}
            <ActivityIndicator size='large' color={colors.primary} ></ActivityIndicator>
      </View>
     :
          <SafeAreaView style={styles.container}>
           {console.log('loadedddd')}
          <ScrollView >
           <View style={styles.spacing}>
               <Text style={styles.font}>Welcome John, </Text>
               <Text style={styles.font}>Introduction about Destination 21. As you all know we are moving ourselves towards the journey of Destination 21, our three year vision to be in the Top 10th percentile in customer experience, employee experience and returns to financial stakeholders Sed quis enim quis turpis lacinia vehicula sit amet sed turpis. Aenean id justo porta,  
Lorem </Text>
          </View>
          <View style={styles.flatListContainer}>
              <FlatList
                 data={data}
                 renderItem={this.renderItem}
                 />
            </View>
            <Text style={styles.featuredStories}>FEATURED STORIES</Text>
            {
            this.state.dataSource.length > 2 ? 
            <View>
            <FlatList
              data={this.state.dataSource.slice(0, this.state.itemCount)}
              renderItem={this.renderStories}
              keyExtractor={this.myKeyExtractor}
              ItemSeparatorComponent={this.renderSeperator}
              scrollEnabled={false}
              initialNumToRender={0}
            />
            <Text style={styles.seeMore} onPress={() => Alert.alert("See more")}>See More...</Text>
            </View>
            :
            <FlatList
            data={this.state.dataSource}
            renderItem={this.renderStories}
            keyExtractor={this.myKeyExtractor}
            ItemSeparatorComponent={this.renderSeperator}
            scrollEnabled={false}
          />
            }
            <this.renderSeperator/>
            <View style={styles.joinNowCard}>
              <Text style={[styles.cardText,{alignSelf:'center'}]}>Question hour with Vinod</Text>
              <View style={{flex:1,flexDirection:'row'}}>
              <MaterialIcon name='mic' size={Platform.OS === 'ios' ?  90 :screenWidth/5} color='white' style={{alignSelf:'center'}}/>
              <View style={{justifyContent:'flex-end',flex:1,flexDirection:'row'}}>
              <Text style={styles.cardText}>JOIN NOW </Text>
              <MaterialIcon name='chevron-right' size={Platform.OS === 'ios' ?  40 :screenWidth/11}  color='white' style={{alignSelf:'flex-end',marginBottom:5}}/>
              </View>
              </View>
            </View>
            <this.renderSeperator/>
            <View styles={{padding: moderateScale(40)}}>
            <Text style={[styles.featuredStories,{margin:moderateScale(20)}]}>WHAT'S NEW</Text>
            
             <FlatList 
                 data={stories}
                 renderItem={this.renderWhatsNew}
                 />
                 </View>
                 
          </ScrollView>
          </SafeAreaView>
    )
          }
  }


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding:  moderateScale(10),
  
  },
  imageContainer: {
    flex: 1,
    flexDirection: 'column',
    marginTop:  moderateScale(10),
    marginBottom:8,
    marginLeft:moderateScale(10),
    marginRight:moderateScale(10)
  },
  image: {
    width: null,
    height: screenWidth/2,
    margin: moderateScale(10)
  },
  description: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft:  moderateScale(10),
    fontSize:myFonts.f16,
    color: colors.primary,
    lineHeight:Platform.OS === 'ios' ?  30 :screenWidth/14
  },
  buttonsRow:
  {
    flexDirection: 'row',
    margin: moderateScale(10),//10
    
  },
  likeAndComment:
  {
    flex: 1,
    flexDirection: 'row',
  },
  share:
  {
    alignSelf:'center',
  },
  buttonContainer:
  {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center', 
  },
  buttonIcon:
  {
    margin:moderateScale(10),//10
    color: colors.primary,
  },
  buttonText:
  {
    fontSize:myFonts.f16 ,
    color: colors.primary,
    marginRight:moderateScale(10),//10
    
  },
  seeMore: {
    alignSelf: "center",
    marginBottom:  moderateScale(20),
    marginTop:5,
    fontSize: screenWidth/26,
    color:colors.primary
  },
  featuredStories:
  {
    fontSize:myFonts.f18,
    marginLeft:  moderateScale(20),
    color: colors.lightGrey,
    fontWeight: 'bold',
    margin:  moderateScale(10)
  },
joinNowCard:
{
  height:screenWidth/2,
  borderWidth:0,
  backgroundColor:colors.card,
  padding : moderateScale(10),
  borderRadius:10,
  alignSelf:'stretch',
  margin: moderateScale(20),
},
cardText:
{
textAlign:'center',
color:'white',
fontWeight:'bold',
fontSize:myFonts.f24,
alignSelf:'flex-end',
margin: moderateScale(10)
},
spacing:
  {
    margin: moderateScale(5)
  },
  font:
  {  color:colors.lightGrey,
     fontSize:myFonts.f16 ,
     margin:  moderateScale(5),
  },
  flatListContainer:
  {
    marginTop: moderateScale(10),
    marginBottom: moderateScale(10)
  }
}
);

