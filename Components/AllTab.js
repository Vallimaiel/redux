import React, { Component } from 'react';
import { View, Dimensions, Text, Button, StyleSheet, FlatList, Image,Platform,ActivityIndicator } from 'react-native'
import {moderateScale, myFonts} from '../res/scaling'
import colors from '../res/colors';


const screenWidth = Dimensions.get('window').width
const stories = [
    {
        key: 'David James',
        subtitle: 'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time: 'Jan 5'
    },
    {
        key: 'Eavid James',
        subtitle: 'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time: 'Jan 5'
    },
    {
        key: 'Favid James',
        subtitle: 'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time: 'Jan 5'
    },
    {
        key: 'Cavid James',
        subtitle: 'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time: 'Jan 5'
    },
    {
        key: 'Bavid James',
        subtitle: 'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time: 'Jan 5'
    },
    {
        key: 'Wavid James',
        subtitle: 'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time: 'Jan 5'
    },
    {
        key: 'Aavid James',
        subtitle: 'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time: 'Jan 5'
    },
    {
        key: 'Javid James',
        subtitle: 'Lorem ipsum dolor sit amet Consectetur  dolor sit amet… ',
        time: 'Jan 5'
    }

];

export default class All extends Component {
   
        constructor(props) {
          super(props);
          isComponentMounted = 'false'
          this.state = {
            usersList: [],
            isLoading: true
          }
        }
        componentDidMount() {
            this.isComponentMounted=true;
            const usersListUrl = 'https://www.yammer.com/api/v1/users.json/?page=3'
             const getMessageUrl = 'https://www.yammer.com/api/v1/messages/in_group/16955277.json?'
        
            fetch(usersListUrl, {
                method: 'GET',
                headers: {
                  Accept: '*/*',
                  Authorization: 'Bearer '+global.token
                }})
                .then((response) => response.json())
                .then((responseJson) => {
                   if(this.isComponentMounted)
                    this.setState({
                      usersList: responseJson,
                      isLoading: false
                    })})
                    .catch((error) => {
                        console.log(error)
                      })
            }
            componentWillUnmount()
            {
              this.isComponentMounted = false
            }
            fetchDp(imageUrl)
            {
                var  url = String(imageUrl)
                url = url.replace("48x48","200x200")
                return url;
            }
                  
    renderItemStories = ({ item, index }) => {
        return (
            <View >
                <View style={styles.container}>
                    <Image source={{ uri: this.fetchDp(item.mugshot_url) }}
                        style={styles.dp} />
                    <View style={{ flex: 1, marginLeft: moderateScale(10), alignSelf: 'center' }}>

                        <View style={{ flexDirection: 'row', flex: 1, alignSelf: 'center', marginTop: moderateScale(5),justifyContent:'center' }}>
                            <View style={styles.nameContainer}>
                                <Text style={styles.name} >{item.full_name} </Text>
                            </View>
                            {/* <View style={styles.timeContainer}>
                                 <Text style={styles.time} >{item.location} </Text> 
                            </View> */}
                        </View>

                        <View style={styles.messageContainer}>
                            <Text numberOfLines={1} style={styles.message}  >{item.job_title} </Text>
                        </View>
                    </View>
                </View>
                <View style={{ borderBottomWidth: 1, borderBottomColor: colors.seperator }}></View>
            </View>
        );
    };
    render() {
        return (
            this.state.isLoading?
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            {console.log('againLoading')}
             <ActivityIndicator size='large' color={colors.primary} ></ActivityIndicator>
       </View>
       :
            <View style={{ flex: 1 }}>
                <FlatList
                    data={this.state.usersList}
                    renderItem={this.renderItemStories}
                    keyExtractor = {(item) => (item.id).toString()}
                />
            </View>
        )
    }
}
const styles = StyleSheet.create({
container:{
     flexDirection: 'row', 
     marginLeft: moderateScale(15), 
     marginRight: moderateScale(15), 
     paddingTop: moderateScale(15), 
     marginBottom: moderateScale(15)
     },
     dp:{
          width: Platform.OS === 'ios' ?  60 : screenWidth/6, 
          height: Platform.OS === 'ios' ?  60 : screenWidth/6, 
          borderRadius: Platform.OS === 'ios' ?  30 : screenWidth/12, 
          alignSelf: 'center' 
        },
        name:
        { fontSize: myFonts.f16,
           fontWeight: 'bold' 
        },
        nameContainer:
        { flexDirection: 'row',
         flex: 1, 
         alignSelf:'center' 
        },
        timeContainer:
        {
         flexDirection: 'row', 
         flex: 1, 
         justifyContent: 'flex-end',
         alignSelf:'center' 
        },
        time:
        {
         fontSize: myFonts.f12, 
         color: colors.darkGrey, 
         paddingLeft: moderateScale(10) 
        },
        messageContainer:
        { flexDirection: 'column', 
          flex: 1,
         justifyContent: 'center'
        },
        message:
        {
        fontSize: myFonts.f14, 
        marginBottom: moderateScale(5), 
        color: colors.darkGrey, 
        textAlign: 'left' 
        }
})