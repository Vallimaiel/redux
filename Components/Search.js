import React, {Component} from 'react';
import { StyleSheet, Text, View,TouchableOpacity,TextInput,Platform,Dimensions} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {moderateScale, myFonts} from '../res/scaling'
const screenWidth = Dimensions.get('window').width;
import colors from '../res/colors'
export default class Search extends Component
{
    render(){
        return(
<View style={{flex:1,padding:moderateScale(20)}}>
<View style={{flexDirection:'row',borderWidth:1,borderColor:'grey'}}>
<MaterialIcon name='search' size={Platform.OS === 'ios' ?  25 : screenWidth/16} color='grey' style={{alignSelf:'center',margin:moderateScale(5)}}/>
    <TextInput placeholder='Type Here' style={{fontSize:Platform.OS === 'ios' ?  18 : screenWidth/24}}/>
    </View>
    <TouchableOpacity style={{backgroundColor:colors.primary,borderRadius:5,marginTop:moderateScale(30),padding:moderateScale(10)}}>
    <Text style={{alignSelf:'center',fontSize:myFonts.f18,color:'white'}}>Search</Text>
    </TouchableOpacity>
</View>
        )
        
    }
}