import React, { Component } from 'react'
import { Text, View, StyleSheet, TextInput, ScrollView, Platform, FlatList, TouchableOpacity, Dimensions,Alert } from 'react-native'
import { scaling, myFonts, moderateScale } from '../res/scaling'
import colors from '../res/colors'
import List from './CustomAttachmentListComponent'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import { PermissionsAndroid } from 'react-native';
import { connect } from 'react-redux'
import { addAttachment, deleteAttachment, postAttachment, clearPost } from '../redux/ActionCreators'

const mapStateToProps = state => {
    {console.log("aaaaaaaaaaaaaaaaaaaaaaaaa",state.attachments.attachmentArray)}
    return {
        attachment: state.attachments,
        
    }
}
const mapDispatchToProps = dispatch => ({
    addAttachment: (fileName, fileType, fileUri) => dispatch(addAttachment(fileName, fileType, fileUri)),
    deleteAttachment: (index) => dispatch(deleteAttachment(index)),
    clearPost:()=>dispatch(clearPost()),
    postAttachment: (message, fileName, fileUri, fileType) => dispatch(postAttachment(message, fileName, fileUri, fileType))
    .then(response=>
       {
           if(response.ok) 
           Alert.alert(
            'Your story has been posted',
            '',
            [
            {
                text:'Ok',onPress:()=>reset()
            }
            ],
            {cancelable:false}
            )

    })
    
    //then(()=>reset())
})
/*navigate = () => {
    console.log("nvigat prnt")
    this.props.navigation.navigate('Stories')
}*/
class PostMessage extends Component {
    constructor(props) {
        super(props)
        this.state = {
            attachmentUri: [],
            attachmentType: [],
            postMsg: '',
            removed: '',
            attachmentArray: [],
            len: 0,
        }
    }
    navigate = () => {
        console.log("nvigat prnt")
        this.props.navigation.navigate('Stories')
    }

    static navigationOptions = ({ navigation }) => {

        return {
            headerTitle: ' Create Post',
            headerTitleStyle: { color: colors.primary, flex: 1, textAlign: 'center', fontSize: myFonts.f18 },
            headerStyle: {
                shadowOpacity: 10,
                backgroundColor: colors.statusBar,
                height: screenWidth / 8
            },
            headerRight: (
                <TouchableOpacity onPress={navigation.getParam('post')} style={[styles.postButton]}>
                    <Text style={{ color: 'white', fontSize: myFonts.f14 }}>POST</Text>
                </TouchableOpacity>)

        }
    }

    componentDidMount() {
        this.props.navigation.setParams({ post: this.postAttachment });
    }

    requestExternalStoragePermission = async () => {
        console.log("kkkk")
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                    title: 'Storage Permission',
                    message:
                        'D21App needs access to your storage ' +
                        'so you can post your attachments',
                    buttonNeutral: 'Ask Me Later',
                    buttonNegative: 'Cancel',
                    buttonPositive: 'OK',
                },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                this.openDocumentPicker()
                console.log("permission granted")

            } else {
                console.log(' Permission denied');
            }
        } catch (err) {
            console.warn(err);
        }
    }


    handleChoosePhoto = () => {
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            if (response) {
                this.props.addAttachment(response.fileName, response.type, response.uri)
            }
        })
    }




    /*postAttachment = () => {
        const form = new FormData();
        
        form.append('body', this.state.postMsg)
        form.append('group_id', 16955277)
       let i=0;
       console.log("lengthfinal",this.state.len)
       while(this.state.len)
       {
        form.append('attachment'+(i+1), {
            uri: this.state.attachmentUri[i],
            type: this.state.attachmentType[i],
            name: this.state.attachmentArray[i],
        });
        this.setState({len:(this.state.len--)})
        console.log("kkkk",this.state.len)
        i++
        console.log("ii",i)
    }
        const PostText = 'https://www.yammer.com/api/v1/messages?for_feed=group&fetch_type=post'
        console.log("msglen",this.state.postMsg.length)
        console.log("attlen",this.state.attachmentArray.length)
        if(this.state.postMsg.length>=1||this.state.attachmentArray.length>=1)
        {
        fetch(PostText, {
            method: 'POST',
            headers: {
                Accept: '*'
                Authorization: 'Bearer'+global.token,
                'Content-Type': 'multipart/form-data',
            },
            
           body: form
        })
            .then(response => alert('Your story has been added'),this.setState({postMsg:'',attachmentArray:[],attachmentUri:[],attachmentType:[]}))
            .catch(error => console.error('Error:', error))
        
            
    }
    else{
        alert('Minimum character of 1 not met')
    }

    }*/

    postAttachment = () => {
        if (this.state.postMsg.length >= 1 || this.props.attachment.attachmentArray.length >= 1) {
            this.props.postAttachment(this.state.postMsg, this.props.attachment.attachmentArray, this.props.attachment.attachmentUri, this.props.attachment.attachmentType)
            reset = (response) => {
                console.log(response)
                this.props.clearPost()
                this.props.navigation.navigate('Stories')
            
            }
        }
        else {
            alert('Minimum character of 1 not met')
        }
    }



    writePost = (value) => {
        this.setState({
            postMsg: value
        })

    }
    openDocumentPicker = () => {
        DocumentPicker.show(
            {
                filetype: [DocumentPickerUtil.allFiles()],
            },
            (error, res) => {
                if (res) {
                    // this.setState({attachmentUri:this.state.attachmentUri.concat(res.uri) })
                    // this.setState({ attachmentType:this.state.attachmentType.concat(res.type) })
                    // this.setState({ attachmentArray:this.state.attachmentArray.concat(res.fileName) })
                    console.log('res : ' + JSON.stringify(res));
                    this.props.addAttachment(res.fileName, res.type, res.uri)
                    this.setState({
                        len: this.state.attachmentArray.length
                    })
                    console.log(this.state.len)
                }
                else
                {
                    console.log(error)
                }
            })
    }
    deleteAttachment = (index) => {
        // this.setState({
        //   removed :this.state.attachmentArray.splice(index,1),
        //  removed :this.state.attachmentUri.splice(index,1),
        //  removed :this.state.attachmentType.splice(index,1),
        //  len:this.state.attachmentArray.length
        // })
        this.props.deleteAttachment(index)

    }


    render() {
        const attachmentList = this.props.attachment.attachmentArray.map((attachment, i) => (
            <List
                key={i}
                attachmentname={attachment}
                onItemPressed={() => this.deleteAttachment(i)}>
            </List>))
        return (
            <ScrollView style={{ flex: 1, marginLeft: moderateScale(15), marginRight: moderateScale(15), }}>
                <TextInput style={styles.textInput} onChangeText={this.writePost} value={this.state.postMsg} multiline={true} placeholder="Add your story" />
                <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                    <TouchableOpacity onPress={this.requestExternalStoragePermission} style={{ marginBottom: moderateScale(10) }}>
                        <Text style={{ color: colors.primary, }} >Attachments
                        <MaterialIcon name='publish' size={Platform.OS === 'ios' ? 18 : screenWidth / 24} style={{ color: colors.primary, }} />
                        </Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1 }}>
                        {attachmentList}
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    textInput:
    {
        width: window.width - 30,
        fontSize: myFonts.f18,
        borderColor: colors.primary,
        borderWidth: 1,
        color: colors.primary,
        marginTop: moderateScale(15),
        marginBottom: moderateScale(10),
    },
    postButton: {
        flex: 1,
        margin: moderateScale(15),
        height: moderateScale(20),
        width: moderateScale(50),
        alignItems: 'center',
        backgroundColor: colors.primary
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(PostMessage)