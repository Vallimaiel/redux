import React, { Component } from 'react';
import { View, TextInput, Text,  StyleSheet, StatusBar,Alert, TouchableOpacity, BackHandler,
    Dimensions,Image,Linking,WebView,ScrollView } from 'react-native'
import {scaling, myFonts, moderateScale} from '../res/scaling'
import colors from '../res/colors'

const LOGIN_URL = 'https://www.yammer.com/office365.com/';

export default class Login extends Component {
    constructor(props) {
        super(props);
        isComponentMounted='false';
        this.state = {
         canGoBack:false,
         checked:'true',
        url:'https://www.yammer.com/dialog/oauth?client_id=xvvJQjkLNnSljKAv7QLDA&redirect_uri=https://ipcloud.tatacommunications.com&response_type=token'
        // url: 'https://www.yammer.com/oauth2/authorize?client_id=xvvJQjkLNnSljKAv7QLDA&response_type=code&redirect_uri=https://ipcloud.tatacommunications.com'
        }
    }
    
    componentDidMount(){
        this.isComponentMounted='true'
        BackHandler.addEventListener('hardwareBackPress', this.backHandler);
   }
   componentWillUnmount(){
       this.isComponentMounted='false'
        BackHandler.removeEventListener('hardwareBackPress', this.backHandler);
   }
   backHandler = () => {
    if (this.state.canGoBack) {
        this.refs["WebView"].goBack();
      }
    else{
      BackHandler.exitApp();
      }
    return true;
   }

    loginFunction=()=> {
       // if(this.isComponentMounted == 'true')
       // this.setState({checked:!this.state.checked})
      this.props.navigation.navigate('MyDrawerNavigation')

}

onMessage =( event )=> {
    // let post = JSON.parse(event.nativeEvent.data);
    // console.log(event.nativeEvent.data)
    // console.log(post)
}
handleNavigationStateChange = navState => { 
  if(this.isComponentMounted == 'true') 
  this.setState({canGoBack:true}) 
  console.log(navState); 
  let redirectCode = `window.location = '${LOGIN_URL}';`;
  if(navState.url == 'https://www.yammer.com/session/new')
  {
  this.refs["WebView"].stopLoading();
  console.log("$$$$$$$$$$$$$")
  this.refs["WebView"].injectJavaScript(redirectCode);
  }
  if(navState.url.substring(0,53) == ('https://ipcloud.tatacommunications.com/#access_token='))
  {
  this.refs["WebView"].stopLoading();
  this.props.navigation.navigate('AppStack')
  global.token = navState.url.substring(53)
  console.log(global.token)
  return false;
  }
  }; 

    render() {
        const injectedJs = `
        window.postMessage(window.location.href);
      `;

        return (
            this.state.checked?  
            <View style={{ backgroundColor: colors.loginBackground, flex: 1 }}>
                <StatusBar
                    backgroundColor={colors.statusBar}
                    barStyle="dark-content"
                />
                <View style={styles.header}> 
                <Image  source={require('../Assets/tatacommlogo.png')}  style={{alignSelf:'center', width:'70%',height:'70%',resizeMode:'contain'}}>
    </Image>
    </View>
                <ScrollView>
                    <Text style={styles.labelText}>Username</Text>
                    <TextInput
                        style={styles.textInput}
                    />
                    <Text style={styles.labelText}>Password</Text>
                    <TextInput
                        style={[styles.textInput,{ marginBottom: moderateScale(15)}]}
                        secureTextEntry={true}
                    />
                    <TouchableOpacity style={styles.button}
                        onPress={this.loginFunction} >
                        <Text style={styles.buttonText}>LOGIN</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
            :
        
            <WebView
            ref="WebView"
            source={{ uri: this.state.url }}
            style={[
              {
                flex: 1
              },
            ]}
          //  injectedJavaScript={injectedJs}
            startInLoadingState
            scalesPageToFit
            javaScriptEnabledAndroid={true}
            javaScriptEnabled={true}
            onShouldStartLoadWithRequest={this.handleNavigationStateChange}
            onNavigationStateChange={this.handleNavigationStateChange}
            onMessage={this.onMessage}
            onLoadStart={() => {
              console.log("LOAD START ");
            }}
            onLoadEnd={() => {
              console.log('LOAD END');
            
            }}
            onError={err => {
              console.log('ERROR');
              console.log(err);
            }}
          />
        )
    }
}
const styles = StyleSheet.create({
    header:
    {
        alignItems: 'center',
        justifyContent: 'center',
        height: '20%',
        backgroundColor: colors.primary,
    },
    labelText:
    { fontSize: myFonts.f14, 
      color: colors.primary, 
      marginLeft: moderateScale(15), 
      marginRight: moderateScale(15), 
      marginTop: moderateScale(30) 
    },
    textInput:
    { height: moderateScale(50),
      width: window.width - 30, 
      fontSize: myFonts.f18, 
      borderColor:  colors.primary,
      borderWidth: 1, 
      color: colors.primary, 
      marginLeft: moderateScale(15), 
      marginRight: moderateScale(15) 
    },
    button:
    {
    alignItems: 'center', 
    justifyContent: 'center', 
    margin: moderateScale(15), 
    height: moderateScale(40), 
    backgroundColor: colors.primary
    },
    buttonText:
    { color: 'white', 
      fontSize: myFonts.f18, }
    

});