import React, { Component } from "react";
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableWithoutFeedback,
  Dimensions,
} from "react-native";
import {scaling, moderateScale, verticalScale, myFonts} from '../res/scaling'
import Video from "react-native-video";
import ProgressBar from "react-native-progress/Bar";
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import colors from '../res/colors'
const screenWidth = Dimensions.get('window').width;
function secondsToTime(time) {
  return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60;
}
export default class Rnvideo extends Component {
  state = {
    paused: true,
    progress: 0,
    duration: 0,
    durationup:0
  }; 
handleMainButtonTouch = () => 
{
    if (this.state.progress >= 1) 
    {
      this.player.seek(0);
    }
    this.setState(state => 
    {
      return {
        paused: !state.paused,
        
      };
    });
};
handleProgressPress = e => 
{
    const position = e.nativeEvent.locationX;
    const progress = (position /(Dimensions.get("window").width/2)) * this.state.durationup;
    const isPlaying = !this.state.paused;
    this.player.seek(progress);
};
handleProgress = progress =>
{
  console.log(progress)
  console.log(this.state.duration) 
    this.setState({
      progress: progress.currentTime / this.state.durationup,
    });
    console.log(this.state.progress) 
};
handleEnd = () => {
    this.setState({ paused: true ,duration:0,progress:0});
    this.player.seek(0);
};
handleLoad = meta => {
  this.player.seek(0);
  console.log(meta)
    this.setState({
      duration: meta.duration,
      durationup:meta.duration
    });  
};
render() {
    const { width } = '100%';
    const height = '85%';
    return (
        <View style={{backgroundColor:'grey'}}>
          <Video
            paused={this.state.paused}
            source={{uri:'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4'}}
            style={{ width, height }}
            onLoad={this.handleLoad}
            resizeMode='stretch'
            onProgress={this.handleProgress}
            onEnd={this.handleEnd}
            ref={ref => {
              this.player = ref;
            }}
          />
          <View style={styles.controls}>
            <TouchableWithoutFeedback onPress={this.handleMainButtonTouch}>
              <MaterialIcon name={!this.state.paused ? "pause" : 'play-arrow'} size={screenWidth/14} color="white" />
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={this.handleProgressPress}>
              <View>
                <ProgressBar
                  progress={this.state.progress}
                  color="white"
                  unfilledColor="rgba(255,255,255,.5)"
                  borderColor="white"
                  width={Dimensions.get("window").width/2}
                  height={screenWidth/34}
                 
                />
              </View>
            </TouchableWithoutFeedback>
            <Text style={styles.duration}>
              {secondsToTime(Math.floor(this.state.progress * this.state.durationup))}
            </Text>
          </View>
        </View>
    );
  }
}
const styles = StyleSheet.create({
    container:
    {
        flex: 1,
        paddingTop: moderateScale(250),
    },
    controls: 
    {
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        height:'20%',
        left: 0,
        bottom: 0,
        right: 0,
        position: "absolute",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-around",
        paddingHorizontal: moderateScale(10),
     },
    mainButton:
    {
        marginRight: moderateScale(15),
    },
    duration: 
    {
        color: "white",
        marginLeft: moderateScale(15),
        fontSize:myFonts.f14
    },
});