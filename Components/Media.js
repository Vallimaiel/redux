import React, {Component} from 'react';
import { StyleSheet, Text, View,Alert,TextInput,SafeAreaView,Platform,FlatList,Image,TouchableOpacity,Dimensions} from 'react-native';
import Swiper from 'react-native-swiper';
import Video from 'react-native-video';
import {AppRegistry} from 'react-native';
import {createDrawerNavigator,DrawerActions} from 'react-navigation'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Rnvideo from './VideoComponent';
const screenWidth = Dimensions.get('window').width
import {moderateScale, myFonts} from '../res/scaling'
import colors from '../res/colors'
const mediaContent = [
  { 
    key: 'Video Title goes here',
    description:'Video Description here Lorem ipsum dolor sit amet, consectetur adipiscing elit'
  }, 
  { 
    key: 'Video Title comes here',
    description:'Video Description here Lorem ipsum dolor sit amet, consectetur adipiscing elit'
  },  
];
export default class Media extends Component{
  constructor(props) {
    super(props);
    this.state = {
      isLiked:false,
    }
  }
  _renderItem = ({ item }) => {
    return (
      <View style={{marginTop:moderateScale(10)}}>
        <View style={styles.headingContainer}>
          <Text style={[styles.buttonText,{fontWeight:'bold'}]}>{item.key}</Text>      
          <Text style={styles.description} >{item.description}</Text>
        </View>
    
          <Swiper  containerStyle={styles.Swiper}>
            <View style={styles.swiperContent}>
              <Image source={{uri : 'https://reactnativecode.com/wp-content/uploads/2018/01/2_img.png'}} style={styles.image} ></Image>
            </View>
            <View style={styles.swiperContent}>
              <Rnvideo/>

            </View>
            <View style={styles.swiperContent}>
              <View style={[{backgroundColor:'yellow'},styles.image]}resizeMode='stretch'></View>
            </View>
        </Swiper>
       
        <View style={styles.buttonsRow}>
          <View style={styles.likeAndComment}>
          <TouchableOpacity style={styles.buttonContainer} onPress={() => { this.setState({isLiked: !this.state.isLiked}),console.log(this.state.isLiked)}}>
            <MaterialIcon name={this.state.isLiked ? 'favorite' : 'favorite-border'} size={Platform.OS === 'ios' ?  18 : screenWidth/24} style={styles.buttonIcon} />
            <Text style={[styles.buttonText,{fontWeight:'bold'}]}>908</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.buttonContainer} onPress={() => Alert.alert('Alert', "456 comments!!")}>
            <MaterialIcon name='comment' size={Platform.OS === 'ios' ?  18 : screenWidth/24} style={styles.buttonIcon}  />
            <Text style={[styles.buttonText,{fontWeight:'bold'}]}>456</Text>
          </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.share} onPress={() => Alert.alert('Alert', "Share to!!")} >
            <Text style={[styles.buttonText,{marginRight:moderateScale(0)}]}> SHARE</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
  static navigationOptions = ({ navigation }) => {

    return {
        headerTitle: 'MEDIA',
        headerTitleStyle:{ color:colors.primary,flex:1,textAlign:'center',fontSize:myFonts.f18},
        headerStyle: {
             shadowOpacity: 10,
             backgroundColor:colors.statusBar,
             height:screenWidth/8
        },
        headerLeft:(
          <TouchableOpacity style={{paddingHorizontal:moderateScale(10)}} onPress={navigation.toggleDrawer}>
            <MaterialIcon
                name="menu"
                size={Platform.OS === 'ios' ?  25 : screenWidth/16}
                color={colors.primary}
            />
            </TouchableOpacity>
        ),
        headerRight: (
           <TouchableOpacity style={{paddingRight:moderateScale(10)}} onPress={()=>navigation.navigate('Search')}>
        <MaterialIcon
            name="search"
            size={Platform.OS === 'ios' ?  25 :screenWidth/16}
            color={colors.primary}
        />
        </TouchableOpacity>)
    }
  }
  render() {
    return(
      <SafeAreaView style={styles.container}>
            <FlatList
              data={mediaContent}
              renderItem={this._renderItem}
              extraData={this.state}
            />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    
  },
  postContainer:
  {
      borderWidth:1,
      backgroundColor:colors.postBackground,
      margin:moderateScale(15),
      flexDirection:'row',
      alignItems:'center',
  },
    Swiper: {
      width:Dimensions.get('window').width,
      height: Dimensions.get('window').height/2.75,
      marginTop:moderateScale(20)
    },
    description: {
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
      fontSize: myFonts.f16,
      marginTop:moderateScale(5)
    },
    buttonsRow:
    {
      flexDirection: 'row',
      marginLeft:moderateScale(25),
      marginRight:moderateScale(25),
      marginBottom:moderateScale(10)
    },
    likeAndComment:
    {
      flex: 1,
      flexDirection: 'row',
    },
    share:
    {
      alignSelf:'center',
    
    },
    buttonContainer:
    {
      flexDirection: 'row',
      alignItems: 'center',
      alignContent: 'center',
    },
    buttonIcon:
    {
      marginRight: moderateScale(10),
      color: colors.primary,
      
    },
    buttonText:
    {
      fontSize:myFonts.f16,
      color: colors.primary,
      marginRight:moderateScale(20),
    },
    headingContainer: {
      marginLeft:moderateScale(25),
      marginRight:moderateScale(25)
    },
    swiperContent:{ 
      flex:1,
      marginLeft:moderateScale(25),
      marginRight:moderateScale(25),
      height:'100%',
    },
    image:{
      width:'100%',
      height:'85%'
  }
  });
