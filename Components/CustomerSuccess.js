import React, {Component} from 'react';
import { StyleSheet, ScrollView,Text, View,TouchableOpacity,FlatList,Image,Dimensions,Platform,SafeAreaView} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { PieChart } from 'react-native-svg-charts'
import { Text as PieText } from 'react-native-svg'
import {moderateScale,myFonts} from '../res/scaling'
import colors from '../res/colors'
const screenWidth =  Dimensions.get('window').width;

const innovationData = [
    {
        key: '1',
        message: ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.',
    },
    {
    key: '2',
    message: ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt. ',
    }]

export default class CustomerSuccess extends Component
{
    constructor(props)
    {
        super(props);
        this.state = {
          dataSource: []
        }
    }
    componentDidMount() {
        console.log('mounting')
    }
    static navigationOptions = ({ navigation }) => {

        return {
            headerTitle: 'CUSTOMER SUCCESS',
             shadowOpacity: 10,
             shadowOpacity: 10,
             headerTitleStyle:{ color:colors.primary,flex:1,textAlign:'center',fontSize:Platform.OS === 'android'? moderateScale(18) : Platform.width > 2000 ? moderateScale(18) : 20},
            headerStyle: {
                 shadowOpacity: 10,
                 backgroundColor:colors.statusBar,
                 height:screenWidth/8
            },
            headerLeft:(
              <TouchableOpacity style={{paddingHorizontal:moderateScale(10)}} onPress={navigation.toggleDrawer}>
                <MaterialIcon
                    name="menu"
                    size={Platform.OS === 'ios' ?  25 : screenWidth/16}
                    color={colors.primary}
                />
                </TouchableOpacity>
            ),
            headerRight: (
               <TouchableOpacity style={{paddingRight:moderateScale(10)}} onPress={()=>navigation.navigate('Search')}>
            <MaterialIcon
                name="search"
                size={Platform.OS === 'ios' ?  25 :screenWidth/16}
                color={colors.primary}
            />
            </TouchableOpacity>)
        }
      }
      renderInnovation=({item})=>
{
    return(
        <View style={{flex:1}}>
        <View style={{flex:1,flexDirection:'row',margin:moderateScale(20)}}>
            <Image style={styles.image}/>
            <Text numberOfLines={3}  style={styles.innovationText}  >
             <Text style={{fontWeight:'bold' }}>Title </Text>
             Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</Text>
            </View>
            <View style={styles.seperator}/>
            </View>
    );
}
    render()
    {
        console.log('rendering in customer success')
        return(
            <SafeAreaView style={styles.container}>
             <ScrollView> 
            <View style={styles.cellContainer}>
            { console.log('returning in customer success')}
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>Customer Success</Text>
                <View style={{flex:1}}>
                <Text style={{fontSize:myFonts.f14,color:colors.lightGrey,textAlign:'right',marginRight:10}}>Target 70</Text> 
                </View>
                </View>

                <View style={{flex:1,flexDirection:'row',marginTop:20,marginLeft:20,marginRight:20,alignSelf:'center'}}>
               <View style={{flex:1}}>
                    <View  style={{flexDirection:'row',alignItems:'center',alignSelf:'center'}}>
                    <MaterialIcon name='person' size={Platform.OS === 'ios' ?  60 :screenWidth/6} color={colors.lightGrey} style={{alignSelf:'flex-start'}}/>
                    <View style={{width:Platform.OS === 'ios' ?  60 :screenWidth/6,height:Platform.OS === 'ios' ?  60 :screenWidth/6,borderRadius:Platform.OS === 'ios' ?  30 :screenWidth/12,borderWidth:2,borderColor:'black',justifyContent:'center'}}>
                    <Text style={{fontWeight:'bold',fontSize:myFonts.f28,color:'black',textAlign:'center',alignSelf:'center'}}>5</Text>
                    </View>
                    </View>
                    <View style={{flex:1,justifyContent:'center'}}>
                    <Text style={{fontSize:myFonts.f16,color:'black',textAlign:'center',alignSelf:'center',margin:moderateScale(10)}}>New Customer</Text>
                    </View>
                    </View>
                    <View style={{height: '60%', width:1, backgroundColor: colors.lightGrey, alignSelf: 'center' ,marginLeft:moderateScale(20),marginRight:moderateScale(20)}}/>
                    <View style={{flex:1}}>
                    <View  style={{flexDirection:'row',alignItems:'center',alignSelf:'center'}}>
                    <MaterialIcon name='sentiment-satisfied' size={Platform.OS === 'ios' ?  60 :screenWidth/6} color='grey' />
                    <Text style={{fontWeight:'bold',fontSize:myFonts.f28,color:'black',textAlign:'center',margin:moderateScale(10)}}>45</Text>
                    </View>
                    <View style={{flex:1,justifyContent:'center'}}>
                    <Text style={{fontSize:myFonts.f16,color:'black',textAlign:'center',alignSelf:'center',margin:moderateScale(10)}}>Current NPS</Text>
                    </View>
                    </View>
                </View>
            <Text style={[styles.bodyText,{paddingBottom:20}]}>Lorem ipsum dolor sit amet, conse ctetur adipisicing</Text>
            </View>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>NPS actuals across LOBS</Text>
                <View style={{flex:1,justifyContent:'center'}}>
                <Text style={{fontSize:myFonts.f14,color:'black',textAlign:'right',fontWeight:'bold'}}>TCL GLOBAL - 65</Text> 
                </View>
                </View>
                <View style={{flex:1,padding:moderateScale(20)}}>
           <PieChartExample/>
           </View>
            </View>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>Top Promoters</Text>
                </View>
                <View style={styles.bodyContainer}>
                <Text style={styles.bodyText}>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis enim quis turpis lacinia vehicula sit amet sed turpis. Aenean id justo porta."</Text> 
 <Text style={styles.bodyText}> - John Doe- (Manager, Mahindra group of companies)</Text>
            </View>
            </View>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>Customer wins</Text>
                </View>
                <View style={styles.bodyContainer}>
                <Text style={styles.bodyText}>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed quis enim quis turpis lacinia vehicula sit amet sed turpis. Aenean id justo porta."</Text> 
            </View>
            </View>
            <View style={styles.cellContainer}>
                <View style={styles.cellHeader}>
                <Text style={styles.leftHeaderText}>Innovation & Transformation</Text>
                </View>
                <View style={[styles.bodyContainer,{paddingBottom:0}]}>
                <FlatList
                    data={innovationData}
                    renderItem={this.renderInnovation}
                />  
            </View>
            </View>
             </ScrollView> 
            </SafeAreaView>
        );
    }
}

class PieChartExample extends React.PureComponent {
    render() {
        const data = [
            {
                key: 1,
                amount: 29,
                svg: { fill: '#18AEE6' },
                label:29
            },
            {
                key: 2,
                amount: 23,
                svg: { fill: '#A9D6E7' },
                label:23
            },
            {
                key: 3,
                amount: 45,
                svg: { fill: '#E2F0F5' },
                label:45
            },
            {

                key: 4,
                amount: 25,
                svg: { fill: '#A9D6E7' },
                label:25
            },
            {
                key: 5,
                amount: 55,
                svg: { fill: '#67CCF1' },
                label:55
            },
            {
                key: 6,
                amount: 55,
                svg: { fill: '#68AEC8' },
                label:55
            }
        ]
      
        const Labels = ({ slices, height, width }) => {
            return slices.map((slice, index) => {
                const { labelCentroid, pieCentroid, data } = slice;
                return (
                    <PieText
                        key={index}
                        x={pieCentroid[ 0 ]}
                        y={pieCentroid[ 1 ]}
                        fill={'black'}
                        textAnchor={'middle'}
                        alignmentBaseline={'central'}
                        fontSize={myFonts.f14}
                    >
                        {data.amount}
                    </PieText>
                )
            })
        }

        return (
            <PieChart
                style={{ height: Platform.OS === 'ios' ?  200 :screenWidth/2}}
                valueAccessor={({ item }) => item.amount}
                data={data}
                padAngle={0}
                innerRadius={0}
                labelRadius={100}
               
            >
                <Labels/>
            </PieChart> 
        )
    } 
}
const styles = StyleSheet.create({
    container:
    {
      flex:1,
    },
    cellContainer: {
        flex:1,
        borderWidth:1,
        borderColor:colors.seperator,
        marginTop:moderateScale(20),
        marginLeft:moderateScale(10),
        marginRight:moderateScale(10)
    },
    cellHeader:
    {
        flex:1,
        flexDirection:'row',
        backgroundColor:colors.seperator,
        padding:moderateScale(15)
    },
    leftHeaderText:
    {   flex:1,
        fontSize:myFonts.f16,
        color:'black',
        fontWeight:'bold'
    },
    bodyContainer:
    {
        flex:1,
        paddingBottom:moderateScale(20),
    },
    bodyText:
    {
        fontSize:myFonts.f16,
        color:'black',
        marginTop:moderateScale(20),
        marginLeft:moderateScale(10),
        marginRight:moderateScale(10)
    },
    image:{
        backgroundColor:'#A9D6E7',
        width:Platform.OS === 'ios' ?  100 :screenWidth/4,
        height:Platform.OS === 'ios' ?  100 :screenWidth/4,
        marginTop:moderateScale(5)
    },
    innovationText:
    {
        flex:1,
        flexWrap:'wrap',
        marginLeft:moderateScale(20),
        alignSelf:'flex-start',
        fontSize:myFonts.f16,
        color:colors.primary,
        lineHeight:Platform.OS === 'ios' ?  30 :screenWidth/14
    },
    seperator:
    { width: '80%',
      height: 1,  
      backgroundColor: colors.seperator, 
      alignSelf: 'center' 
    }
})