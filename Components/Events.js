import React, {Component} from 'react';
import { StyleSheet, Text, View,TouchableOpacity,ScrollView,Dimensions,Platform,SafeAreaView} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { createMaterialTopTabNavigator,NavigationActions,createBottomTabNavigator } from 'react-navigation'
import {moderateScale, myFonts,scale} from '../res/scaling'
import colors from '../res/colors';
const screenWidth=Dimensions.get('window').width;


class UpcomingScreen extends React.Component {
   
    render() {
        console.log('rendering')
      return (
            <ScrollView style={styles.container}>
             {console.log('returning1')}
        <View style={styles.cellsContainer}>
            <View style={styles.dateContainer}>
             <Text style={styles.dateText} textAlign='center'>16</Text>
            <Text style={styles.dateText} textAlign="center">OCT</Text>
            </View>
            <View style={styles.eventsCell}>
            <View>
            <Text style={[styles.eventType,{color:colors.primary}]}>Event Name here</Text>
            <Text style={styles.eventType}>Event type</Text>
            </View>
            {console.log('returning2')}
            <Text  numberOfLines={2} style={styles.eventDescription}>Description here Lorem ipsum dolor sit amet, consectetur  </Text>
            </View> 
        </View>
        <View style={styles.seperator}/>
        <View style={styles.cellsContainer}>
            <View style={styles.dateContainer}>
             <Text style={styles.dateText} textAlign='center'>30</Text>
            <Text style={styles.dateText} textAlign="center">OCT</Text>
            </View>
            <View style={styles.eventsCell}>
            <View>
            <Text style={[styles.eventType,{color:colors.primary}]}>Event Name here</Text>
            <Text style={styles.eventType}>Event type</Text>
            </View>
            {console.log('returning3')}
            <Text  numberOfLines={2} style={styles.eventDescription}>Description here Lorem ipsum dolor sit amet, consectetur  </Text>
            </View> 
        </View>
        <View style={styles.seperator}/>
        {console.log('returning4')}
        </ScrollView>

      );
    }
  }
  
  class PastScreen extends React.Component {
    render() {
      return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          <Text>Settings!</Text>
        </View>
      );
    }
  } 
  const EventsTabNavigator = createMaterialTopTabNavigator({
    UPCOMING: UpcomingScreen,
    PAST: PastScreen,
  }, {
     
    tabBarPosition:'top',
    tabBarOptions: {
      activeTintColor: 'black',
      inactiveTintColor: 'grey',
      labelStyle: {
        fontSize: myFonts.f16,
      },
      style:{
        backgroundColor:'white',
        marginTop:moderateScale(10)
       },
      indicatorStyle:
      {
          backgroundColor:colors.primary
      }
    },
    },
);
  
  
export default class Events extends Component
{
    static router = EventsTabNavigator.router
    static navigationOptions = ({ navigation }) => {

        return {
            headerTitle: 'EVENTS',
            headerTitleStyle:{ color:colors.primary,flex:1,textAlign:'center',fontSize:myFonts.f18},
            headerStyle: {
                 shadowOpacity: 10,
                 backgroundColor:colors.statusBar,
                 height:screenWidth/8
            },
            headerLeft:(
              <TouchableOpacity style={{paddingHorizontal:moderateScale(10)}} onPress={navigation.toggleDrawer}>
                <MaterialIcon
                    name="menu"
                    size={Platform.OS === 'ios' ?  25 : screenWidth/16}
                    color={colors.primary}
                />
                </TouchableOpacity>
            ),
            headerRight: (
               <TouchableOpacity style={{paddingRight:moderateScale(10)}} onPress={()=>navigation.navigate('Search')}>
            <MaterialIcon
                name="search"
                size={Platform.OS === 'ios' ?  25 :screenWidth/16}
                color={colors.primary}
            />
            </TouchableOpacity>)
        }
      }
    
    
render()
{
    return(
        <SafeAreaView style={{flex:1}}>
       <EventsTabNavigator navigation = {this.props.navigation}/>
       </SafeAreaView>
    );
}
}
const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    cellsContainer:
    {
        flex:1,
        flexDirection:'row',
        margin:moderateScale(20)
    },
    dateContainer:
    {  
        borderColor:colors.seperator,
        borderWidth:2,
        height:'auto',
        justifyContent:'center',
        aspectRatio:1/1
    },
    dateText:
    {
        color:colors.lightGrey,
        alignSelf:'center',
        textAlign:'center',
        fontSize:myFonts.f28,
        fontWeight:'bold'
    },
    eventsCell:
    {
        flex:1,
        marginLeft:moderateScale(20)
    },
    eventType:{
    textAlign:'left',
    fontSize:myFonts.f16,
    },
    eventDescription:
    {
        //alignSelf:'center',
        fontSize:myFonts.f16,
        marginTop:moderateScale(8)
    },
    seperator:{
        width: '100%', 
        height: 5, 
        backgroundColor: colors.seperator,
        alignSelf: 'center'
    },
    test:
    {  
        flex:1,
        padding:moderateScale(10)
    }
})