import React from 'react';
import {Text,View,StyleSheet,TouchableOpacity,Platform,Dimensions} from 'react-native'
import colors from '../res/colors';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { scaling, myFonts, moderateScale } from '../res/scaling'

const List = (props) =>
{
    return(
    <View style={styles.list} >
        <Text style={{flex:1}}>{props.attachmentname}</Text>
        <MaterialIcon name='clear' onPress={props.onItemPressed} size={Platform.OS === 'ios' ? 18 : screenWidth / 24} style={{justifyContent:'flex-end' ,color: colors.primary,marginLeft:moderateScale(10),marginRight:moderateScale(10),padding:moderateScale(5)}} />
    </View>);
}
const styles=StyleSheet.create({
    list:{
        flexDirection:'row',
        marginBottom:moderateScale(5),
        alignItems:'center',
        backgroundColor:colors.statusBar
    }
})
export default List;