import React, {Component} from "react";
import {createStackNavigator,createDrawerNavigator,NavigationActions,DrawerActions,DrawerItems} from 'react-navigation'
import{
    View,
    Text,
    StyleSheet,StatusBar,
    SafeAreaView,Dimensions,ScrollView,TouchableOpacity,Image,Platform
}from "react-native";
import colors from '../res/colors'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import FeaturesComponent from "./FeaturesComponent";
import NotificationCell from "./NotificationCell";
import Events from "./Events";
import MyProfile from "./MyProfile";
import CustomerSuccess from "./CustomerSuccess";
import Media from "./Media";
import StoriesCell from "./Stories";
import Group from "./D21Group";
import navHeader from "./navHeader";
import {moderateScale, myFonts} from '../res/scaling'
import PostMessage from "./PostMessage";
const screenWidth = Dimensions.get('window').width



class CustomComponent extends Component
{
    constructor(props) {
     
        super(props);
        this.state = {
          menu:global.currentScreen,
        }
      }
      componentDidMount()
      {
        console.log("MOUNTEDDD")
      }
      componentWillUnMount()
      {
        console.log(" UNNNNNNNN  MOUNTEDDD")
      }
      stateUpdate()
      {
        this.setState({menu:global.currentScreen})
      }
        navigateToScreen = (route) =>  {
      //this.props.navigation.state.routes[this.props.navigation.state.index].routeName
       const navigateAction = NavigationActions.navigate({
         routeName: route
       }); 
      // this.setState({menu:global.currentScreen})
        this.props.navigation.dispatch(navigateAction);
        this.props.navigation.dispatch(DrawerActions.closeDrawer())
      }
  render() {
    return (
     
    <View style={{flex:1}}>
     <StatusBar
      backgroundColor={colors.statusBar}
      barStyle="dark-content"
  />
    <View style = {styles.container1}>
    <Image  source={require('../Assets/tatacommlogo.png')}  style={{alignSelf:'center', width:'80%',resizeMode:'contain',
    height: '80%'}}/>
    </View> 
     <ScrollView style = {styles.container2}>
        <TouchableOpacity  style={[styles.drawerItemContainer,{marginBottom:moderateScale(-5)},(global.currentScreen =='Summary' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Summary')
        }}>
           <MaterialIcon name='explore' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>DESTINATION 21 SUMMARY</Text>
           </TouchableOpacity>
           <TouchableOpacity  style={[styles.subItemContainer,(global.currentScreen =='CustomerSuccess' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('CustomerSuccess')
        }}>
           <MaterialIcon name='explore' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>CUSTOMER SUCCESS</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.subItemContainer} >
           <MaterialIcon name='explore' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>EMPLOYEE SUCCESS</Text>
           </TouchableOpacity>

           <TouchableOpacity style={styles.subItemContainer} >
           <MaterialIcon name='explore' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text  style={styles.drawerItemText}>REVENUE & PRODUCTIVITY IN THE YEAR 2019</Text>
           </TouchableOpacity>
           <TouchableOpacity style={styles.subItemContainer} >
           <MaterialIcon name='explore' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>EBIDTA & ROCE</Text>
           </TouchableOpacity>
           <TouchableOpacity style={[styles.subItemContainer,{marginBottom:moderateScale(10)}]} >
           <MaterialIcon name='explore' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>FINAL DESTINATION</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(global.currentScreen =='Stories' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Stories')
        }}>
           <MaterialIcon name='dashboard' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>STORIES</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(global.currentScreen =='D21Group' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('D21Group')
        }}>
           <MaterialIcon name='forum' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>D21 GROUP</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(global.currentScreen =='Event' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Event')
        }}>
           <MaterialIcon name='explore' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>EVENTS</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(global.currentScreen =='Media' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Media')
        }}>
           <MaterialIcon name='photo' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>MEDIA</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(global.currentScreen =='Notification' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('Notification')
        }}>
           <MaterialIcon name='mail' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>NOTIFICATIONS</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity style={styles.drawerItemContainer} >
           <MaterialIcon name='videocam' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>DOWNLOADS</Text>
           </TouchableOpacity>
           <View style={styles.ItemSeperator}></View>
           <TouchableOpacity  style={[styles.drawerItemContainer,(global.currentScreen =='MyProfile' ? styles.active : {})]}
        onPress={()=>{this.navigateToScreen('MyProfile')
        }}>
           <MaterialIcon name='settings' size={Platform.OS === 'ios' ?  25 :screenWidth/16} style={styles.drawerItemIcon}/>
           <Text style={styles.drawerItemText}>MY PROFILE</Text>
           </TouchableOpacity>
    </ScrollView> 
   
    </View>
    )}
}

const SummaryStack = createStackNavigator(
    {
    first:FeaturesComponent,
    },
    {
      initialRouteName : 'first',
     // navigationOptions: navHeader ,
      cardStyle:{
        backgroundColor:colors.backgroundColor
        },
     
    }
  ) 
  const NotificationStack = createStackNavigator(
    {
    Notification:NotificationCell
    },
    {
      initialRouteName : 'Notification',
      cardStyle:{
        backgroundColor:colors.backgroundColor
        },
    }
  ) 
  const EventsStack = createStackNavigator(
    {
    EventsList:Events
    },
    {
      initialRouteName : 'EventsList',
      cardStyle:{
        backgroundColor:colors.backgroundColor
        },
    }
  ) 
  const CustomerSuccessStack = createStackNavigator(
    {
    CustomerSuccess:CustomerSuccess
    },
    {
      initialRouteName : 'CustomerSuccess',
      cardStyle:{
        backgroundColor:colors.backgroundColor
        },
    }
  ) 
  const MyProfileStack = createStackNavigator(
    {
     MyProfile:MyProfile
    },
    {
      initialRouteName : 'MyProfile',
      cardStyle:{
        backgroundColor:colors.backgroundColor
        },
    }
  ) 
  const MediaStack = createStackNavigator(
    {
     Media:Media
    },
    {
      initialRouteName : 'Media',
      cardStyle:{
        backgroundColor:colors.backgroundColor
        },
    }
  ) 
  const StoriesStack = createStackNavigator(
    {
     Stories:StoriesCell,
     Post:PostMessage
    },
    {
      initialRouteName : 'Stories',
      cardStyle:{
        backgroundColor:colors.backgroundColor
        },
    }
  ) 
  const D21GroupStack = createStackNavigator(
    {
     D21Group:Group
    },
    {
      initialRouteName : 'D21Group',
      cardStyle:{
        backgroundColor:colors.backgroundColor
        },
    }
  ) 
  function getActiveRouteName(navigationState) {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // dive into nested navigators
    // if (route.routes) {
    //   console.log(route.routes+"88888888")
    //   return getActiveRouteName(route);
    // }
    
    return route.routeName;
  } 
  

const DrawerNavigator = createDrawerNavigator(
    {
   Notification:NotificationStack,
   Summary:SummaryStack,
   Event:EventsStack,
   CustomerSuccess:CustomerSuccessStack,
   MyProfile:MyProfileStack,
   Media:MediaStack,
   Stories:StoriesStack,
   D21Group:D21GroupStack
    },
{
        initialRouteName:'Summary',
        contentComponent:CustomComponent,
        drawerWidth:screenWidth *0.7,
        contentOptions:
        {
          activeTintColor:colors.drawerItem
        } 
    },
)

 class DrawerNavigation extends Component
{
  constructor(props)
  {
    console.log("constructor of drawer")
    super(props);
    this.state={
     // obj:new CustomComponent()
    }
  }
  render()
  {
    return(
      <DrawerNavigator 
      onNavigationStateChange={(prevState,currentState) => {
        const routes =  getActiveRouteName(currentState)
        currentScreen = routes;
     this.state.obj.stateUpdate()
       const prevScreen = getActiveRouteName(prevState);
         console.log("currentScreen  : " + routes);
       console.log("PreviousScreen  : " + prevScreen);  
     }}
    />
    )
  }
}

// const  AppContainer = createAppContainer(drawerNavigator)
 export default DrawerNavigator

 const styles = StyleSheet.create({
     container:{
         backgroundColor: 'white',
         color:'white',
         flex:1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    container1:{
        backgroundColor: colors.primary,
        height: '20%',
        justifyContent:'center'
    },
    container2:{
        backgroundColor:colors.drawerBackground
    },
    drawerItemContainer:
    {
      flex:1,
      flexDirection:'row',
      padding:moderateScale(7),
      alignItems:'center',
     
    },
    drawerItemText:
    {   flex:1,
        flexWrap: 'wrap',
        color:'white',
        fontSize:myFonts.f14,
    },
    drawerItemIcon:
    {
        color:'white',
        margin:moderateScale(10)
    },
    subItemContainer:
    { 
        flex:1,
        flexDirection:'row',
        paddingLeft:moderateScale(35),
        alignItems:'center',
        paddingRight:moderateScale(15),
    },
    ItemSeperator:
    {
        backgroundColor:colors.primary,
        height:0.5
    },
    active:
    {
        backgroundColor:colors.drawerItem
    }
});
