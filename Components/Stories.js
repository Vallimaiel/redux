import React, {Component} from 'react';
import { StyleSheet,Modal, Text, View,Alert,ActivityIndicator,Platform,FlatList,Image,TouchableOpacity,Dimensions,SafeAreaView} from 'react-native';
import {AppRegistry} from 'react-native';
import {createDrawerNavigator,DrawerActions} from 'react-navigation'
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
const screenWidth = Dimensions.get('window').width;
import colors from '../res/colors'
import {moderateScale, myFonts} from '../res/scaling'
import ModelComponent from './ModelComponent';

export default class StoriesCell extends Component{
  constructor(props) {
    super(props);
    isComponentMounted = 'false',
    this.state = {
      isliked:[],
      Messages: [],
      UsersMap:[],
      LikeCounts:[],
      userDetails:[],
      isFullScreen: false,
      isLoading: true,
      isLikeUpdated:true
    }
  }
  componentDidMount() {
    this.isComponentMounted=true;
    const getUserDetails = 'https://www.yammer.com/api/v1/users/current.json'
     const getMessageUrl = 'https://www.yammer.com/api/v1/messages/in_group/16955277.json?'
    
     fetch(getUserDetails, {
      method: 'GET',
      headers: {
        Accept: '*/*',
        Authorization: 'Bearer '+global.token
      }})
      .then((response) => response.json())
      .then((responseJson) => {
         if(this.isComponentMounted)
          this.setState({
            userDetails: responseJson,
            // isLoading: false
          })})
          .catch((error) => {
              console.log(error)
            })
  
       fetch(getMessageUrl,
                    {
                        method: 'GET',
                        headers: {
                          Accept: '*/*',
                          Authorization: 'Bearer '+global.token,
                        }})
                        .then((response) => response.json())
                        .then((responseJson) => {
                           if(this.isComponentMounted)
                           {
                            this.setState(prevState=>({
                              Messages: responseJson.messages, 
                              UsersMap:responseJson.references,
                            }))
                            this.isLiked()
                          }
                          }
                            )
                            .catch((error) => {
                                console.log(error)
                              })
                             
                            }
  componentWillUnmount()
  {
    this.isComponentMounted = false
  }
  findSender(senderId)
  {
  return ( this.state.UsersMap.filter((user) => user.id == senderId)[0].full_name)
  }
  getDisplayPicture(senderId)
  {
    return ( this.state.UsersMap.filter((user) => user.id == senderId)[0].mugshot_url.toString().replace("48x48","200x200"))
  }
 formatDate(date)
 {
    formattedDate = new Date(date);

    console.log(formattedDate)
    console.log(formattedDate.toString().substring(8,10))
    hour = Number(formattedDate.toString().substring(16,18))
    min = formattedDate.toString().substring(19,21)
    if(hour >12)
    {
      return  (formattedDate.toString().substring(4,7)+"-"+formattedDate.toString().substring(8,10)+
       " "+ (hour-12).toString()+':'+min + ' pm');
    }
   return  (formattedDate.toString().substring(4,7)+"-"+formattedDate.toString().substring(8,10)+" "+
    hour +':'+min + ' am');
 }
  isLiked=(item = this.state.Messages) =>
 {
  console.log(item)
   const likes=[]
   const count=[]
   console.log(item.length)
   for(k=0;k<item.length;k++)
   {
     console.log('k : '+k)

     if(item[k].liked_by.count==0)
     likes[k] = false

   for(i=0;i<item[k].liked_by.count;i++)
   {
    console.log('i : '+i)
   if(item[k].liked_by.names[i].user_id == this.state.userDetails.id)
   {
     console.log(k+" : "+'true')
     likes[k] = true
     break;
   }
   else{
   console.log(k+" : "+'false')
    likes[k] = false
   }
   
 }
 count[k] = item[k].liked_by.count
 } 
 
 this.setState({isliked:likes,LikeCounts:count,isLoading:false})
 console.log("__________________",this.state.LikeCounts)
}

 likePost(messageId,isLiked,index)
 {
  
   console.log('liked'+ isLiked)
   const likeUrl = 'https://www.yammer.com/api/v1/messages/liked_by/current.json?message_id='+messageId

   if(this.state.isLikeUpdated)
   {
    this.setState({isLikeUpdated:false})
   fetch(likeUrl,
                {
                    method: isLiked ? 'DELETE' :'POST',
                    headers: {
                      Accept: '*/*',
                      Authorization: 'Bearer '+global.token, 
                    }})
                    .then((response) => {
                      console.log("_________________________________________________________________"+response.status);
                      console.log(isLiked);
                       if((this.isComponentMounted) && (response.status == '200' || response.status == '201'))
                       {
                        let likesTemp = [...this.state.isliked];     // create the copy of state array
                        likesTemp[index] = !likesTemp[index];
                        let countTemp = [...this.state.LikeCounts];
                        if(isLiked) 
                        countTemp[index] = countTemp[index]-1;
                        else
                        countTemp[index] = countTemp[index]+1;
                        this.setState({
                        isliked:likesTemp,
                        LikeCounts:countTemp,
                        isLikeUpdated:true,
                        })
                        console.log(this.state.isliked[index])
                        console.log(this.state.count)
                      }
                      })
                        .catch((error) => {
                            console.log(error)
                          })
                        }
                      }
                      
                         fetchImage=(index)=> 
                        {
                          if(this.state.Messages[index].attachments.length > 0 && this.state.Messages[index].attachments[0].image)
                          {
                            return this.state.Messages[index].attachments[0].image.url
                          }
                          return false;
                        }
  
  _renderItem = ({ item,  index }) => {
    return (
      <View>
      <View style={{margin:moderateScale(15)}}>
        <View style={{flexDirection:'row',flex:1,alignItems:'center'}}>
          <View style={{flexDirection:'row',flex:1,alignItems:'center'}}>
            <Image source={{uri : this.getDisplayPicture(item.sender_id)}}  style={styles.dp} /> 
               
            <Text style={[styles.buttonText,{marginLeft:moderateScale(10),fontWeight:'400'}]}>{this.findSender(item.sender_id)}</Text>
          </View>  
          <View style={{flexDirection:'row',flex:1,justifyContent:'flex-end',alignItems:'center'}}>
            <MaterialIcon name='query-builder' size={Platform.OS === 'ios' ?  18 : screenWidth/24} style={styles.buttonIcon}/>
            <Text style={[styles.buttonText,{marginRight:moderateScale(0),fontSize: myFonts.f14}]}>{this.formatDate(item.created_at)}</Text>
            
          </View>    
        </View>   
        {
          item.body.plain == ''?
          <View/>
          :
          <Text style={styles.description} >{item.body.plain}</Text>
        }  
        {
          
          this.fetchImage(index) == false ?
        <View style={{marginBottom:moderateScale(20),backgroundColor:'red'}}/>
        :
        <ModelComponent src = {this.fetchImage(index)}/>
        
        }
        <View style={styles.buttonsRow}>
          <View style={styles.likeAndComment}>
          <View style={styles.buttonContainer} >
          <TouchableOpacity style={{paddingRight: moderateScale(7),}} onPress={()=>this.likePost(item.id,this.state.isliked[index],index)}>
            {console.log('index'+this.state.isliked[index])}
            <MaterialIcon name={this.state.isliked[index] == true? 'favorite' : 'favorite-border'} size={Platform.OS === 'ios' ?  18 : screenWidth/24} style={styles.buttonIcon} />
            </TouchableOpacity>
            <Text style={[styles.buttonText,{fontWeight:'400'}]}>{this.state.LikeCounts[index]}</Text>
          </View>
          
          <View style={styles.buttonContainer} onPress={() => Alert.alert('Alert', "456 comments!!")} >
          <TouchableOpacity style={{paddingRight: moderateScale(7),}}>
            <MaterialIcon name='comment' size={Platform.OS === 'ios' ?  18 : screenWidth/24} style={styles.buttonIcon}  />
            </TouchableOpacity>
            <Text style={[styles.buttonText,{fontWeight:'400'}]}>456</Text>
            </View>
         
          </View>
          <TouchableOpacity style={styles.share} onPress={() => Alert.alert('Alert', "Share to!!")} >
            <Text style={[styles.buttonText,{marginRight:moderateScale(0),fontWeight:'400'}]}>SHARE</Text>
          </TouchableOpacity>
      </View>
      </View>
        <View style={{ width: '90%', height: 1, backgroundColor: colors.seperator, alignSelf: 'center' }}>
      </View>
      </View>
    )
            }

  static navigationOptions = ({ navigation }) => {
    return {
        headerTitle: 'STORIES',
        headerTitleStyle:{ color:colors.primary,flex:1,textAlign:'center',fontSize:myFonts.f18},
        headerStyle: {
             shadowOpacity: 10,
             backgroundColor:colors.statusBar,
             height:screenWidth/8
        },
        headerLeft:(
          <TouchableOpacity style={{paddingHorizontal:moderateScale(10)}} onPress={navigation.toggleDrawer}>
            <MaterialIcon
                name="menu"
                size={Platform.OS === 'ios' ?  25 : screenWidth/16}
                color={colors.primary}
            />
            </TouchableOpacity>
        ),
        headerRight: (
           <TouchableOpacity style={{paddingRight:moderateScale(10)}} onPress={()=>navigation.navigate('Search')}>
        <MaterialIcon
            name="search"
            size={Platform.OS === 'ios' ?  25 :screenWidth/16}
            color={colors.primary}
        />
        </TouchableOpacity>)
    }
  }
  render() {
    return(
     
      this.state.isLoading?
      <SafeAreaView style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      {console.log('againLoading')}
       <ActivityIndicator size='large' color={colors.primary} ></ActivityIndicator>
 </SafeAreaView>
 :
      <SafeAreaView style={styles.container}>
      <TouchableOpacity style={styles.postContainer} onPress ={()=>this.props.navigation.navigate('Post')}>
      <MaterialIcon name='edit' size={Platform.OS === 'ios' ?  18 : screenWidth/24} style={{alignSelf:'center',margin:moderateScale(5)}} color={colors.primary}/>
       <Text style={{color:colors.lightGrey,margin:moderateScale(5),fontSize:myFonts.f16,fontWeight:"100"}}>Write a post...</Text>
       </TouchableOpacity>
            <FlatList
              data={this.state.Messages}
              extraData = {this.state}
              renderItem={this._renderItem}
              keyExtractor = {(item) => (item.id).toString()}
            />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  postContainer:
  {
      borderWidth:1,
      backgroundColor:colors.postBackground,
      margin:moderateScale(15),
      flexDirection:'row',
      alignItems:'center'
  },
    image: {
      width:null,
      height: Dimensions.get('window').height/3,
      alignContent: 'stretch',
      backgroundColor:'white',
      marginTop:moderateScale(20),
      marginBottom:moderateScale(20)
    },
    description: {
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
      fontSize:myFonts.f16,
      marginTop:moderateScale(10)
    },
    buttonsRow:
    {
      flexDirection: 'row',
      //marginBottom:moderateScale(10),
    },
    likeAndComment:
    {
      flex: 1,
      flexDirection: 'row',
    },
    share:
    {
      alignSelf:'center',
    },
    buttonContainer:
    {
      flexDirection: 'row',
      alignItems: 'center',
      alignContent: 'center',
    },
    buttonIcon:
    {
      color: '#3D86C6',
    },
    buttonText:
    {
      fontSize: myFonts.f16,
      color: colors.primary,
      marginRight:moderateScale(20)
    },
    dp:
    {
    width: Platform.OS === 'ios' ?  40 : screenWidth/11, 
    height: Platform.OS === 'ios' ?  40 : screenWidth/11,
    borderRadius:Platform.OS === 'ios' ?  20 : screenWidth/22,
    alignSelf:'center'
    }
  } 
);
