import React, {Component} from 'react';
import { StyleSheet, Text, View,TouchableOpacity,FlatList,Image,ScrollView,Dimensions,Platform,SafeAreaView} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import {moderateScale, myFonts} from '../res/scaling'
import colors from '../res/colors'
const screenWidth = Dimensions.get('window').width;
const notification = [
    {
        key: '1',
        message: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim',
        time: '1hr ago'
    },
    {
    key: '2',
    message: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim ',
    time: '1hr ago'
    }]
    

export default class NotificationCell extends Component
{   
    constructor(props){
        super(props)
        isComponentMounted = 'false'
        this.state={
            notificationsList:[],
            notificationStatus:[],
            counts:0
            
        }
    }
    componentDidMount() {
        this.isComponentMounted=true;
        const notificationsUrl = 'https://www.yammer.com/api/v1/streams/notifications.json'
        fetch(notificationsUrl, {
        method: 'GET',
        headers: {
        Accept: '*/*',
        Authorization: 'Bearer '+global.token
        }})
        .then((response) => response.json())
        .then((responseJson) => {
            if(this.isComponentMounted)
            //console.log((responseJson.items))
            this.setState({
                notificationsList:responseJson.items
                
            })
           // console.log(("array",this.state.notificationsList))
        })
        .catch((error) => {
        console.log(error)
        })
        
        } 
        
        componentWillUnmount()
        {
        this.isComponentMounted = false
        } 
         
        
    static navigationOptions = ({ navigation }) => {

        return {
            headerTitle: 'NOTIFICATIONS',
            headerTitleStyle:{ color:colors.primary,flex:1,textAlign:'center',fontSize:myFonts.f18},
            headerStyle: {
                 shadowOpacity: 10,
                 backgroundColor:colors.statusBar,
                 height:screenWidth/8
            },
            headerLeft:(
              <TouchableOpacity style={{paddingHorizontal:moderateScale(10)}} onPress={navigation.toggleDrawer}>
                <MaterialIcon
                    name="menu"
                    size={Platform.OS === 'ios' ?  25 : screenWidth/16}
                    color={colors.primary}
                />
                </TouchableOpacity>
            ),
            headerRight: (
               <TouchableOpacity style={{paddingRight:moderateScale(10)}} onPress={()=>navigation.navigate('Search')}>
            <MaterialIcon
                name="search"
                size={Platform.OS === 'ios' ?  25 :screenWidth/16}
                color={colors.primary}
            />
            </TouchableOpacity>)
        }
      }
    notificationcount()
    {
       this.setState({count:this.state.count++})
       console.log(this.state.count)
    }
    
renderNotification = ({item,index})=>
{
    
    return(
        <View style={{flex:1}}>
        { status= item.unseen}
        {(status=='0') ?console.log("unseen false"):null}
        <View style={[status?{backgroundColor:colors.statusBar}:{backgroundColor:''},styles.notificationContainer]}>
            <Image style={styles.NotificationImg}/>
            <View style={styles.NotificationContent}>
            <Text style={styles.time}>{item.time}</Text>
            <Text  numberOfLines={2} style={styles.text}>{item.message}</Text>
            </View> 
        </View>
        <View style={styles.seperator}/>
       </View>
    );
}

render()
{
    return(
        <SafeAreaView style={{flex:1}}>
        <ScrollView>
        <FlatList
                    data={this.state.notificationsList}
                    renderItem={this.renderNotification} 
                    keyExtractor = {(item) => (item.id).toString()}
                />
         </ScrollView>
         </SafeAreaView>
        
    )
}
}
const styles = StyleSheet.create({
    notificationContainer: {
      flex: 1,
      flexDirection:'row',
      margin:moderateScale(20)
    },
    NotificationImg: {
        backgroundColor:'grey',
        width:Platform.OS === 'ios' ?  80 : screenWidth/5,
        height:Platform.OS === 'ios' ?  80 : screenWidth/5,
        marginTop:moderateScale(5)
    },
    NotificationContent: {
        flex:1,
        marginLeft:moderateScale(10)
    },
    time: {
        textAlign:'left',
        color:colors.lightGrey,
        fontSize:myFonts.f12,
        fontStyle:'italic'
    },
    text: {
        alignSelf:'center',
        fontSize:myFonts.f14,
        color:colors.primary,
        lineHeight:Platform.OS === 'ios' ?  30 : screenWidth/14
    },
    seperator: {
        width: '100%',
        height: 1,
        backgroundColor: colors.seperator,
        alignSelf: 'center' 
    }
})