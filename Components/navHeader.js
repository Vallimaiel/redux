import React, { Component } from 'react';
import {Platform,TouchableOpacity,Dimensions} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
const screenWidth=Dimensions.get('window').width;

export default class navHeader extends Component{
    constructor(props)
    { super(props);
      this.state = {
        navigation : this.props.navigation,
      }
    }
    render()
    {
        return{
            headerTitle: 'D21 SUMMARY',
            headerTitleStyle:{ color:'#3D86C6',flex:1,textAlign:'center',fontSize:Platform.OS === 'ios' ?  null : screenWidth/22},
            headerStyle: {
                 shadowOpacity: 10,
                // elevation:10,
                 backgroundColor:'#ebf7ff',
                 height:screenWidth/8
            },
            headerLeft:(
              <TouchableOpacity style={{padding:10}} onPress={navigation.toggleDrawer}>
                <MaterialIcon
                    name="menu"
                    size={Platform.OS === 'ios' ?  25 : screenWidth/16}
                    color='#3D86C6'
                />
                </TouchableOpacity>
            ),
            headerRight: (
               <TouchableOpacity style={{padding:20}} onPress={()=>navigation.navigate('Search')}>
            <MaterialIcon
                name="search"
                size={Platform.OS === 'ios' ?  25 :screenWidth/16}
                color='#3D86C6'
            />
            </TouchableOpacity>)
        }
    }
}