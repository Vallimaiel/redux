
import React, {Component} from 'react';
import {SafeAreaView,Modal,Image,TouchableOpacity,View,StyleSheet,Dimensions} from 'react-native'
import {moderateScale, myFonts} from '../res/scaling'
export default class All extends Component{
    constructor(props)
    {
        super(props);
        this.state = {
            isFullScreen:false,
            src:props.src
        }
    }
  render()
  {
    return(
        <SafeAreaView>
        <TouchableOpacity onPress={() => { this.setState({ isFullScreen: true }) }}>
        <Image source={{ uri: this.props.src }} resizeMode="contain" style={styles.image} />
    </TouchableOpacity>
    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
    <Modal style = {{alignContent:'center',backgroundColor:'black'}} backgroundColor='black' animationType='fade' visible={this.state.isFullScreen} onRequestClose={() => { this.setState({ isFullScreen: false }) }}>
        <Image source={{ uri: this.props.src }} style={styles.modelImage} resizeMode='contain' />
    </Modal>
    </View>
    </SafeAreaView>
   )  
  }
}
const styles = StyleSheet.create({
   
      image: {
        width:null,
        height: Dimensions.get('window').height/3,
        alignContent: 'stretch',
        backgroundColor:'white',
        marginTop:moderateScale(20),
        marginBottom:moderateScale(20)
      },
      modelImage: {
          alignItems:'center',
        width:null,
        height: Dimensions.get('window').height/3,
        alignContent: 'stretch',
        backgroundColor:'white',
        marginTop:moderateScale(20),
        marginBottom:moderateScale(20)
      },
    } 
  );