import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, FlatList, Image, ScrollView, Dimensions, Platform, SafeAreaView } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { moderateScale, myFonts } from '../res/scaling';
const screenWidth = Dimensions.get('window').width;
import colors from '../res/colors'
export default class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [
        { key: 'PERSONAL INFORMATION' },
        { key: 'NOTIFICATIONS' },
        { key: 'PREFERENCES' },
        { key: 'TERMS & CONDITION' }
      ],
    }
  }

  static navigationOptions = ({ navigation }) => {

    return {
      headerTitle: 'MY PROFILE',
      headerTitleStyle:{ color:colors.primary,flex:1,textAlign:'center',fontSize:myFonts.f18},
      headerStyle: {
           shadowOpacity: 10,
           backgroundColor:colors.statusBar,
           height:screenWidth/8
      },
      headerLeft:(
        <TouchableOpacity style={{paddingHorizontal:moderateScale(10)}} onPress={navigation.toggleDrawer}>
          <MaterialIcon
              name="menu"
              size={Platform.OS === 'ios' ?  25 : screenWidth/16}
              color={colors.primary}
          />
          </TouchableOpacity>
      ),
      headerRight: (
         <TouchableOpacity style={{paddingRight:moderateScale(10)}} onPress={()=>navigation.navigate('Search')}>
      <MaterialIcon
          name="search"
          size={Platform.OS === 'ios' ?  25 :screenWidth/16}
          color={colors.primary}
      />
      </TouchableOpacity>)
    }
  }
  _renderItem = ({ item }) => {
    return (
      <TouchableOpacity style={styles.optionsContainer}>

        <Text style={styles.options}>
          {item.key}
        </Text>
        <MaterialIcon name='chevron-right' size={Platform.OS === 'ios' ? 30 : screenWidth / 14} style={{ marginRight: moderateScale(20), alignSelf: 'center', color: colors.darkGrey }} />
      </TouchableOpacity>
    )
  }

  renderSeperator = () => {
    return (
      <View style={{ width: '100%', height: 5, backgroundColor: colors.seperator, alignSelf: 'center' }}>
      </View>
    )
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <ScrollView >
          <View style={styles.infoContainer}>
            <Image style={styles.dp} />
            <View style={styles.info}>
              <Text style={styles.name}>Welcome Jane</Text>
              <Text style={styles.designation}>Manager - DIAS, Mumbai. </Text>
            </View>
            <MaterialIcon name='edit' size={Platform.OS === 'ios' ? 30 : screenWidth / 14} color={colors.primary} style={styles.icon} />
          </View>
          <View style={{ flex: 1, flexDirection: 'row', margin: moderateScale(10), alignSelf: 'center' }}>
            <View style={styles.activityContainer}>
              <MaterialIcon name='people' size={Platform.OS === 'ios' ? 30 : screenWidth / 14} color={colors.primary} style={styles.icon} />
              <Text style={styles.activity}>20 </Text>
              <Text style={styles.activity}>Followers</Text>
            </View>
            <View style={styles.seperator} />

            <View style={styles.activityContainer}>
              <MaterialIcon name='comment' size={Platform.OS === 'ios' ? 30 : screenWidth / 14} color={colors.primary}  style={styles.icon} />
              <Text style={styles.activity}>173 </Text>
              <Text style={styles.activity}>Comments</Text>
            </View>
            <View style={styles.seperator} />

            <View style={styles.activityContainer}>
              <MaterialIcon name='edit' size={Platform.OS === 'ios' ? 30 : screenWidth / 14} color={colors.primary} style={styles.icon} />
              <Text style={styles.activity}>4 </Text>
              <Text style={styles.activity}>Posts</Text>
            </View>
          </View>
          <this.renderSeperator />
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            ItemSeparatorComponent={this.renderSeperator}
            scrollEnabled={false}
          />
          <this.renderSeperator />
        </ScrollView>

        <Text style={styles.signOut}>SIGN OUT</Text>

      </SafeAreaView>

    )
  }
}
const styles = StyleSheet.create({
  signOut:
  {
    color: colors.primary,
    fontSize:myFonts.f16,
    textAlign: 'center',
    margin: moderateScale(10),
    fontWeight: 'bold'
  },
  infoContainer: {
    flex: 1,
    flexDirection: 'row',
    margin: moderateScale(20)
  },
  dp: {
    width: Platform.OS === 'ios' ? 80 : screenWidth / 5,
    height: Platform.OS === 'ios' ? 80 : screenWidth / 5,
    borderRadius: Platform.OS === 'ios' ? 40 : screenWidth / 10,
    backgroundColor: 'grey'
  },
  info: {
    flex: 1,
    flexDirection: 'column',
    alignSelf: 'center',
    marginLeft: moderateScale(10)
  },
  name:
  {
    fontSize:myFonts.f18,
    fontWeight: 'bold'
  },
  designation:
  {
    fontSize: myFonts.f14,

    color: colors.lightGrey
  },
  icons:
  {
    alignSelf: 'center'
  },
  activityContainer:
  {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center'
  },
  activity:
  {
    fontSize:myFonts.f16,
    color: colors.lightGrey
  },
  seperator:
  {
    height: '80%',
    width: 1,
    backgroundColor: 'grey',
    alignSelf: 'center'
  },
  optionsContainer:
  {
    flex: 1,
    flexDirection: 'row',
    paddingTop: moderateScale(20), 
    paddingBottom: moderateScale(20) 
  },
  options:
  {
    flex: 1,
    fontSize:myFonts.f16,
    color: 'black',
    marginLeft: moderateScale(20),
    alignSelf: 'center'
  }
})