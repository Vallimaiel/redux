import * as ActionTypes from '../ActionTypes'
export const attachments = (state={attachmentUri:[],attachmentType:[],attachmentArray:[]},action)=>{
    switch(action.type)
    {
        case ActionTypes.ADD_ATTACHMENT:
            return{...state,...state.attachmentUri.push(action.fileUri),...state.attachmentType.push(action.fileType),...state.attachmentArray.push(action.fileName)}
        case ActionTypes.DELETE_ATTACHMENT:
            return{...state,...state.attachmentUri.splice(action.payload,1),...state.attachmentType.splice(action.payload,1),...state.attachmentArray.splice(action.payload,1)}
        case ActionTypes.CLEAR_POST:
            return{...state,attachmentUri:[],attachmentArray:[],attachmentType:[]}
        default:
            return state
    }
}