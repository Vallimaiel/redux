import * as ActionTypes from './ActionTypes';
import {Alert} from 'react-native'
import { PostUrl } from '../Shared/baseUrl';
import { withNavigation } from 'react-navigation';


/*export const postAttachment = (dishId,rating,comment,author)=>(dispatch)=>{
    const payload = {
        dishId: dishId,
        rating: rating,
        author: author,
        comment: comment
    };
    payload.date = new Date().toISOString();
    
    return fetch(baseUrl + 'comments', {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
            'Content-Type': 'application/json'
        },
        credentials: 'same-origin'
    })
    .then(response => {
        if (response.ok) {
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        throw error;
    })
    .then(response =>response.json())
    .then(response => setTimeout(() => { dispatch(addComment(response)) }, 2000))
    .catch(error => { console.log('Post comments ' + error.message);
     });
}*/

export const postAttachment = (message,fileName,fileUri,fileType)=>(dispatch)=>{
    const form = new FormData();
        
        form.append('body', message)
        form.append('group_id', 16955277)
        let i=0;
        let length=fileName.length
        while(length)
       {
        form.append('attachment'+(i+1), {
            uri: fileUri[i],
            type: fileType[i],
            name: fileName[i],
        });
        length--
        console.log("kkkk",length)
        i++
        console.log("ii",i)
    }
    return fetch(PostUrl , {
        method: 'POST',
        headers: {
            Accept: '*/*',
            Authorization: 'Bearer'+global.token,
            'Content-Type': 'multipart/form-data',
        }, 
       body: form   
    })
   /* .then(response => {
        if (response.ok) {
            alert('Your story has been added')
            //dispatch(clearPost())
            return response;
        }
        else {
            var error = new Error('Error ' + response.status + ': ' + response.statusText);
            error.response = response;
            throw error;
        }
    },
    error => {
        throw error;
    })
    .then(response =>console.log(response))
    .catch(error => { console.log('Post Attachment ' + error.message);
     });*/
}

export const addAttachment = (fileName,fileType,fileUri)=>({
    type:ActionTypes.ADD_ATTACHMENT,
    fileName:fileName,
    fileType:fileType,
    fileUri:fileUri,
    
})

export const deleteAttachment = (index)=>({
    type:ActionTypes.DELETE_ATTACHMENT,
    payload:index
})

export const clearPost = ()=>({
    type:ActionTypes.CLEAR_POST,
    
})