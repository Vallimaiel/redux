
   const colors= {
        backgroundColor: '#ffffff',
        primary: '#3D86C6',
        statusBar:'#ebf7ff',
        loginBackground:'#F2F9FE',
        d21Labels:'#E3E1E1',
        lightGrey:'#8E8E8E',
        darkGrey:'#666479',
        seperator:'#E3E1E1',
        card:'#0073aa',
        postBackground:'#EFEFEF',
        drawerItem:'#34495e',
        drawerBackground: '#202020'
    }
export default colors