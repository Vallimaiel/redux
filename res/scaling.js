import { Dimensions,Platform } from 'react-native';
const { width, height } = Dimensions.get('window');

//Guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => width / guidelineBaseWidth * size;
const verticalScale = size => height / guidelineBaseHeight * size;
const moderateScale = (size, factor = 0.5) => size + ( scale(size) - size ) * factor;

export {scale, verticalScale, moderateScale,myFonts};

const myFonts=
{
f12: Platform.OS === 'android'? moderateScale(12) : screenWidth > 2000 ? moderateScale(12) : 14,
f14: Platform.OS === 'android'? moderateScale(14) : screenWidth > 2000 ? moderateScale(14) : 16,
f16: Platform.OS === 'android'? moderateScale(16) : screenWidth > 2000 ? moderateScale(16) : 18,
f18: Platform.OS === 'android'? moderateScale(18) : screenWidth > 2000 ? moderateScale(18) : 20,
f20: Platform.OS === 'android'? moderateScale(20) : screenWidth > 2000 ? moderateScale(20) : 22,
f22: Platform.OS === 'android'? moderateScale(22) : screenWidth > 2000 ? moderateScale(22) : 24,
f24: Platform.OS === 'android'? moderateScale(24) : screenWidth > 2000 ? moderateScale(24) : 26,
f26: Platform.OS === 'android'? moderateScale(26) : screenWidth > 2000 ? moderateScale(26) : 28,
f28: Platform.OS === 'android'? moderateScale(28) : screenWidth > 2000 ? moderateScale(28) : 30,
} 