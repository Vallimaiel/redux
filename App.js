import React, { Component } from 'react';
import { Platform, StyleSheet, Text, Dimensions, View } from 'react-native';
import { createStackNavigator, createSwitchNavigator } from 'react-navigation'
import MyDrawerNavigation from './Components/MyDrawerNavigation';
import Events from './Components/Events';
import Search from './Components/Search';
import Login from './Components/Login';
import navHeader from './Components/navHeader';
import { myFonts } from './res/scaling';
import colors from './res/colors';
import CustomComponent from './Components/MyDrawerNavigation';
import { Provider } from 'react-redux';
import { ConfigureStore } from './redux/ConfigureStore';
import {PersistGate} from 'redux-persist/es/integration/react'

const {persistor,store}  = ConfigureStore();



const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});
const screenWidth = Dimensions.get('window').width
global.loadedd = 'false';
const AuthNavigator = createStackNavigator(
  {
    Home: {
      screen: Login,
      navigationOptions: {
        header: null
      }
    },
  }
)

const AppNavigator = createStackNavigator(
  {
    MyDrawerNavigation:
    {
      screen: MyDrawerNavigation,
      navigationOptions: {
        header: null
      },
    },
    Search:
    {
      screen: Search,
      navigationOptions: ({ navigation }) => ({

        headerTitle: 'Search Results',
        headerTitleStyle: { color: colors.primary, flex: 0.8, textAlign: 'center', fontSize: myFonts.f18 },
        headerStyle: {
          shadowOpacity: 10,
          backgroundColor: colors.statusBar,
          height: screenWidth / 8,
          size: Platform.OS === 'ios' ? 25 : screenWidth / 16
        },
        headerTintColor: colors.primary,
      }),
    },
  },
  {
    initialRouteName: 'MyDrawerNavigation',
    //headerMode:'None'  
  }
);

//const AppContainer = createAppContainer(
const SwitchNavigator = createSwitchNavigator(
  {
    AuthStack: AuthNavigator,
    AppStack: AppNavigator
  },
  {
    initialRouteName: 'AuthStack',
  }
);

function getActiveRouteName(navigationState) {
  if (!navigationState) {
    return null;
  }
  const route = navigationState.routes[navigationState.index];
  // dive into nested navigators
  if (route.routes && i < 2) {
    i++;
    console.log(i);
    return getActiveRouteName(route);
  }
  return route.routeName;
}
let i = 0;
export default class App extends React.Component {
  constructor(props) {
    super(props);
    global.screenWidth = Dimensions.get('window').width;
    global.token = '19153-t9MexgmU7nz3tlF4bTIFAQ';

    global.loadedd = 'false';
    global.data = [];
    global.currentScreen = 'Summary';
  }
  render() {
    //Text.defaultProps = { allowFontScaling: false };
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <SwitchNavigator onNavigationStateChange={(prevState, currentState) => {
            i = 0;
            const routes = getActiveRouteName(currentState)
            global.currentScreen = routes;
            console.log("currentScreen  : " + routes);
          }} />
        </PersistGate>
      </Provider>
    )
  }
}

